<?php

namespace Drupal\mermaid_filter\Plugin\Filter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;

/**
 * @Filter(
 *   id = "filter_mermaid",
 *   title = @Translation("Mermaid Filter"),
 *   description = @Translation("Render Mermaid graphs."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_IRREVERSIBLE,
 *   settings = {
 *     "theme" = "default",
 *     "filter_html_help" = 1,
 *     "filter_html_nofollow" = 0
 *   },
 *   weight = -10,
 * )
 */
class FilterMermaid extends FilterBase {

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    // Replace content of the [mermaid] blocks.
    $text = preg_replace_callback('@\[mermaid\](.+?)\[/mermaid\]@s', function(array $matches) {
      return self::processMermaid($matches[1]);
    }, $text);

    $result = new FilterProcessResult($text);

    // Attach Mermaid JS libraries.
    $result->setAttachments([
      'library' => [
         'mermaid/mermaid',
         'mermaid/mermaid-init',
      ],
    ]);

    return $result;
  }

  /**
   * Callback to process a single [mermaid] block.
   */
  public function processMermaid($text) {
    $result = "<pre class='mermaid'>\n";
    $result .= "%%{init: { 'theme': '{$this->settings['theme']}' } }%%";
    $result .= $text;
    $result .= "</pre>\n";
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form['theme'] = [
      '#type' => 'radios',
      '#title' => $this->t('Theme'),
      '#options' => [
        'default' => $this->t('Default'),
        'dark' => $this->t('Dark'),
        'forest' => $this->t('Forest'),
      ],
      '#default_value' => $this->settings['theme'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function tips($long = FALSE) {
    return self::help($long);
  }

  /**
   * Provide help text.
   */
  static function help($long = FALSE) {
    if ($long) {
      return 'With <a href="https://mermaid.js.org/intro/#diagram-types" target="_blank" rel="noopener noreferrer">Mermaid</a> you can create inline graphs:<br/>'
          . '<code>[mermaid]<br/>graph LR<br/>&nbsp;&nbsp;Alice === Bob<br/>[/mermaid]<br/></code>'
          . 'For more info, see <a href="http://drupal.org/project/mermaid">Mermaid Integration</a>.';
    }
    else {
      return 'Use <a href="https://mermaid.js.org/intro/#diagram-types" target="_blank" rel="noopener noreferrer">Mermaid</a> to generate an inline graph.';
    }
  }

}
