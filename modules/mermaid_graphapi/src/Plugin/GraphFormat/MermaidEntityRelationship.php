<?php

namespace Drupal\mermaid_graphapi\Plugin\GraphFormat;

use Drupal\graphapi\Plugin\GraphFormat\GraphFormatBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;

/**
 * The Mermaid entity relationship graph format.
 *
 * Supports the following options in the render element '#options' array:
 *  - theme: The Mermaid theme.
 *
 * Supports the following attributes on graph vertices:
 *  - title: The label of the vertex.
 *
 * Supports the following optional attributes on graph edges:
 *  - title: The label of the edge.
 *  - first-multiple: A boolean indicating whether the first entity on the edge
 *    is multiple-valued. Defaults to FALSE.
 *  - first-required: A boolean indicating whether the first entity on the edge
 *    is required. Defaults to FALSE.
 *  - second-multiple: A boolean indicating whether the second entity on the
 *    edge is multiple-valued. Defaults to FALSE.
 *  - second-required: A boolean indicating whether the second entity on the
 *    edge is required. Defaults to FALSE.
 *
 * @GraphFormat(
 *   id = "mermaid_er",
 *   label = @Translation("Mermaid entity relationship"),
 *   engine = "mermaid",
 *   default_options = {
 *     "theme" = "default",
 *   },
 *   supported_attributes = {
 *     "edge" = {
 *       "title"
 *     },
 *   },
 * )
 */
class MermaidEntityRelationship extends GraphFormatBase {

  /**
   * Array of valid keys in the init directive.
   */
  protected const INIT_KEYS = [
    'theme',
  ];

  /**
   * {@inheritdoc}
   */
  public function defaultOptionsForm(array $form, FormStateInterface $form_state, EntityInterface $entity) {
    $form = parent::defaultOptionsForm($form, $form_state, $entity);

    $form['theme'] = [
      '#type' => 'radios',
      '#title' => $this->t('Theme'),
      '#options' => [
        'default' => $this->t('Default'),
        'dark' => $this->t('Dark'),
        'forest' => $this->t('Forest'),
      ],
      '#default_value' => $entity->getOption('theme'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function preProcess(&$variables) {
    $graph = $variables['graph'];

    $mermaid_graph_text = [];

    // Assemble init options if any are present.
    $init_options = [];
    foreach (static::INIT_KEYS as $init_key) {
      if (isset($variables['options'][$init_key])) {
        $init_options[$init_key] = $variables['options'][$init_key];
      }
    }
    $init_strings = [];
    array_walk($init_options, function ($value, $key) use (&$init_strings) {
      $init_strings[] = "'$key': '$value'";
    });

    if ($init_strings) {
      $mermaid_graph_text[] = "%%{init: { " . implode(', ', $init_strings) . " } }%%";
    }

    $mermaid_graph_text[] = 'erDiagram';

    foreach ($graph->getEdges() as $edge) {
      $start = $edge->getVertexStart()->getAttribute('title') ?? $edge->getVertexStart()->getId();
      $end = $edge->getVertexEnd()->getAttribute('title') ?? $edge->getVertexEnd()->getId();

      // An empty edge label must be specified as a double-quoted string.
      $edge_label = '"' . ($edge->getAttribute('title') ?? '') . '"';

      // Assemble the connector ends.
      if (empty($edge->getAttribute('first-multiple'))) {
        $first_connector = '|';
      }
      else {
        $first_connector = '}';
      }

      if (empty($edge->getAttribute('first-required'))) {
        $first_connector .= 'o';
      }
      else {
        $first_connector .= '|';
      }

      if (empty($edge->getAttribute('second-required'))) {
        $second_connector = 'o';
      }
      else {
        $second_connector = '|';
      }

      if (empty($edge->getAttribute('second-multiple'))) {
        $second_connector .= '|';
      }
      else {
        $second_connector .= '{';
      }

      $mermaid_graph_text[] = "$start $first_connector--$second_connector $end : $edge_label";
    }

    $variables['content'] = implode("\n", $mermaid_graph_text);
  }

}
