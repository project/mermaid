<?php

namespace Drupal\mermaid_graphapi\Plugin\GraphEngine;

use Drupal\graphapi\Plugin\GraphEngine\GraphEngineBase;

/**
 * The Mermaid graph engine.
 *
 * @GraphEngine(
 *   id = "mermaid",
 *   label = @Translation("Mermaid"),
 * )
 */
class Mermaid extends GraphEngineBase {

  /**
   * {@inheritdoc}
   */
  public function preRender($element): array {
    $element['#attached']['library'][] = 'mermaid/mermaid';
    $element['#attached']['library'][] = 'mermaid/mermaid-init';

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function preProcess(&$variables) {
    $variables['attributes']->addClass('mermaid');
  }

}
